package javatest.basic;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

//Given a square matrix, calculate the absolute difference between the sums of its diagonals.
//For example, the square matrix  is shown below:
//        1 2 3
//        4 5 6
//        9 8 9
//The left-to-right diagonal = 1 + 5 + 9 = 15. The right to left diagonal = 3 + 5 + 9 = 17
//Their absolute difference is |15 - 17| = 2
//https://www.hackerrank.com/challenges/one-week-preparation-kit-diagonal-difference/problem?isFullScreen=true&h_l=interview&playlist_slugs%5B%5D=preparation-kits&playlist_slugs%5B%5D=one-week-preparation-kit&playlist_slugs%5B%5D=one-week-day-two

public class DiagonalDifference {
    /*
     * Complete the 'diagonalDifference' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts 2D_INTEGER_ARRAY arr as parameter.
     */

    public static int diagonalDifference(List<List<Integer>> arr) {
        // Write your code here
        int size = arr.size();
        int sumLeftDiagonal = 0;
        int sumRightDiagonal = 0;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (i == j) {
                    sumLeftDiagonal += arr.get(i).get(j);
                }
                if (i + j == size - 1) {
                    sumRightDiagonal += arr.get(i).get(j);
                }
            }
        }
        return Math.abs(sumLeftDiagonal - sumRightDiagonal);
    }

    public static void main(String[] args) throws IOException {
//        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
//        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));
//
//        int n = Integer.parseInt(bufferedReader.readLine().trim());
//
//        List<List<Integer>> arr = new ArrayList<>();
//
//        IntStream.range(0, n).forEach(i -> {
//            try {
//                arr.add(
//                        Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
//                                .map(Integer::parseInt)
//                                .collect(Collectors.toList())
//                );
//            } catch (IOException ex) {
//                throw new RuntimeException(ex);
//            }
//        });
//
//        int result = diagonalDifference(arr);
//
//        bufferedWriter.write(String.valueOf(result));
//        bufferedWriter.newLine();
//
//        bufferedReader.close();
//        bufferedWriter.close();
        List<Integer> list1 = Arrays.asList(11, 2, 4, 6);
        List<Integer> list2 = Arrays.asList(4, 5, 6, 8);
        List<Integer> list3 = Arrays.asList(10, 8, -12, -3);
        List<Integer> list4 = Arrays.asList(1, 5, 0, -9);
        List<List<Integer>> list = Arrays.asList(list1, list2, list3, list4);
        System.out.println(diagonalDifference(list));

    }
}
