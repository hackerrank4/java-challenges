package javatest.basic;

import java.util.HashMap;
import java.util.Map;

public class Roman2Integer {
    public static void main(String[] args) {
        //https://leetcode.com/problems/roman-to-integer/
        String s = "MCMXCIV";
        Map<String, Integer> map = new HashMap<>();
        map.put("I", 1);
        map.put("V", 5);
        map.put("X", 10);
        map.put("L", 50);
        map.put("C", 100);
        map.put("D", 500);
        map.put("M", 1000);

        Map<String, String> exception = new HashMap<>();
        exception.put("I", "VX" );
        exception.put("X", "LC" );
        exception.put("C", "DM" );

        int start = 0;
        int sum = 0;
        while (start < s.length()) {
            String currentChar = s.substring(start, start++ + 1);
            int value = map.get(currentChar);

            boolean isSpecial = false;
            String nextChar = "";
            if (start!= s.length() && exception.containsKey(currentChar)) {
                int temp = start + 1;
                nextChar = s.substring(start, temp);
                if (exception.get(currentChar).contains(nextChar)) {
                    isSpecial = true;
                }
            }
            if (!isSpecial) {
                sum += value;
            } else {
                sum += map.get(nextChar) - value;
                // ignore the next character
                start++;
            }
        }

        System.out.println(sum);
    }
}
