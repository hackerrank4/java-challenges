package javatest.basic;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

// Given five positive integers, find the minimum and maximum values that can be calculated by summing exactly four of the five integers.
// Then print the respective minimum and maximum values as a single line of two space-separated long integers.

// Beware of integer overflow! Use 64-bit Integer.
// https://www.hackerrank.com/challenges/one-week-preparation-kit-mini-max-sum/problem?isFullScreen=true&h_l=interview&playlist_slugs%5B%5D=preparation-kits&playlist_slugs%5B%5D=one-week-preparation-kit&playlist_slugs%5B%5D=one-week-day-one
public class MiniMaxSum {
    public static void miniMaxSum(List<Integer> arr) {
        arr.sort(Comparator.naturalOrder());
        BigInteger min = BigInteger.ZERO, max = BigInteger.ZERO ;
        for (int i = 0; i < arr.size(); i++) {
            if (i < arr.size() - 1) {
                min = min.add(BigInteger.valueOf(arr.get(i)));
            }
            if (i != 0) {
                max = max.add(BigInteger.valueOf(arr.get(i)));
            }
        }
        System.out.println(min + " " + max);

    }

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(256741038, 623958417, 467905213, 714532089, 938071625);
        miniMaxSum(list);
    }
}
