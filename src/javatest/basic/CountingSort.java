package javatest.basic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

// https://www.hackerrank.com/challenges/one-week-preparation-kit-countingsort1/problem?isFullScreen=true&h_l=interview&playlist_slugs%5B%5D=preparation-kits&playlist_slugs%5B%5D=one-week-preparation-kit&playlist_slugs%5B%5D=one-week-day-two
public class CountingSort {
    public static List<Integer> countingSort(List<Integer> arr) {
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            result.add(Collections.frequency(arr, i));
        }
        return result;
    }

    public static void main(String[] args) {
        List<Integer> arr = Arrays.asList(1, 1, 3, 2, 1);
        System.out.println(countingSort(arr));
    }
}
