package javatest.basic;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class InterestingPhoneNumber {
    public static void main(String[] args) {
        // interesting if a single digit is occurring at least 4 times
        // a phone number is always 10 digits length
        String phoneNumber = "0909891111";
        int i = 0,j = 1;
        List<Integer> list = new ArrayList<>();
        while (j <= phoneNumber.length()) {
            list.add(Integer.valueOf(phoneNumber.substring(i++, j++)));
            list.sort(Comparator.naturalOrder());
        }

        int start = list.get(0);
        int count = 1;
        for (int k = 1; k < list.size(); k++) {
            if (start == list.get(k)) {
                count++;
            } else {
                start = list.get(k);
                count = 1;
            }
            if (count == 4) {
                System.out.println("yeah - interesting phone number. ");
                break;
            }
        }
    }
}
