package javatest.basic;

import java.util.Arrays;
import java.util.List;

// Given an array of integers, where all elements but one occur twice, find the unique element.
// arr = [1,2,3,4,3,2,1]
// The unique element is 4
// https://www.hackerrank.com/challenges/one-week-preparation-kit-lonely-integer/problem?isFullScreen=true&h_l=interview&playlist_slugs%5B%5D=preparation-kits&playlist_slugs%5B%5D=one-week-preparation-kit&playlist_slugs%5B%5D=one-week-day-two
public class LonelyInteger {
    public static int lonelyinteger(List<Integer> a) {
        for (int i = 0; i < a.size(); i++) {
            int count = 0;
            for (int j = 0; j < a.size() ; j++) {
                if (a.get(i) == a.get(j)) {
                    count ++;
                }
            }
            if (count == 1) {
                return a.get(i);
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        System.out.println(lonelyinteger(Arrays.asList(0,0,1,2,1)));
        System.out.println(lonelyinteger(Arrays.asList(1,2,3,4,3,2,1)));
    }
}
