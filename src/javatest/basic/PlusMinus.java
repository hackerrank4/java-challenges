package javatest.basic;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;


// Given an array of integers, calculate the ratios of its elements that are positive, negative, and zero.
// Print the decimal value of each fraction on a new line with  places after the decimal.
// example arr = [-4, 3, -9, 0, 4, 1]
// 0.500000  -> proportion of positive values
// 0.333333  -> proportion of negative values
// 0.166667  -> proportion of zeros

// https://www.hackerrank.com/challenges/one-week-preparation-kit-plus-minus/problem?isFullScreen=true&h_l=interview&playlist_slugs%5B%5D=preparation-kits&playlist_slugs%5B%5D=one-week-preparation-kit&playlist_slugs%5B%5D=one-week-day-one
public class PlusMinus {
    public static void plusMinus(List<Integer> arr) {
        int positiveCount, negativeCount, zeroCount;
        Predicate<? super Integer> predicate = number -> number > 0;
        positiveCount = (int) arr.stream().filter(predicate).count();
        predicate = number -> number < 0;
        negativeCount = (int) arr.stream().filter(predicate).count();
        predicate = number -> number == 0;
        zeroCount = (int) arr.stream().filter(predicate).count();
        System.out.println(String.format("%.6f", positiveCount * 1.0f/arr.size()));
        System.out.println(String.format("%.6f", negativeCount * 1.0f/arr.size()));
        System.out.println(String.format("%.6f", zeroCount * 1.0f/arr.size()));
    }

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(-4, 3, -9, 0, 4, 1);
        plusMinus(list);
    }
}
